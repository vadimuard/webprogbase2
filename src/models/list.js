const mongoose = require("mongoose");

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

const ListSchema = new mongoose.Schema({
    title: { type: String, default: "untitled" },
    text: { type: String },
    createdAt: { type: Date, default: Date.now() },
    usersGroup: { type: Array }
});

const ListModel = mongoose.model('List', ListSchema)

class List {
    constructor(title, createdAt, text, usersGroup) {
        this.title = title;
        this.text = text;
        this.createdAt = createdAt;
        this.usersGroup = usersGroup;
    }

    static getById(id) {
        return ListModel.findById(id);
    }

    static getAllPaginated(userId, page, search_string) {
        return ListModel.find({ usersGroup: { _id: userId }, "title": { "$regex": search_string, "$options": 'i' } }).skip(page * 8).limit(8);
    }

    static insert(notes_list) {
        return new ListModel(notes_list).save();
    }

    static update(updated_list) {
        return ListModel.updateOne({ _id: updated_list._id }, updated_list);
    }

    static delete(id) {
        return ListModel.findByIdAndDelete(id);
    }

    static getCountOfAllLists(userId, search_string) {
        return ListModel.countDocuments({
            usersGroup: { _id: userId },
            "title": { "$regex": search_string, "$options": 'i' }
        });
    }
}

module.exports = List;