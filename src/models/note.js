const mongoose = require("mongoose");

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

const NoteSchema = new mongoose.Schema({
    title: { type: String, default: "untitled" },
    text: { type: String },
    userId: {type: String, required:true },
    createdAt: { type: Date, default: Date.now() },
    reminder: { type: Date },
    authorId: { type: Number, default: Math.random().toFixed(0) },
    fileUrl: { type: String, default: null },
    listId: { type: String, default: null }
});

const NoteModel = mongoose.model('Note', NoteSchema)

class Note {
    constructor(title, text, createdAt, reminder, authorId, fileUrl, listId, userId) {
        this.text = text;
        this.title = title;
        this.reminder = reminder;
        this.authorId = authorId;
        this.createdAt = createdAt;
        this.fileUrl = fileUrl;
        this.userId = userId;
        this.listId = listId;
    }

    static getById(id) {
        return NoteModel.findById(id);
    }

    static getAllPaginated(userId, page, search_string) {
        return NoteModel.find({ "userId": userId, "title": { "$regex": search_string, "$options": 'i' }, listId: null })
            .skip(page * 8).limit(8);
    }

    static getAllByList(list_id) {
        return NoteModel.find({ listId: list_id });
    }

    static insert(note) {
        return new NoteModel(note).save();
    }

    static update(note) {
        return NoteModel.findByIdAndUpdate(note._id, note);
    }

    static deleteMany(id) {
        return NoteModel.deleteMany({ listId: id });
    }

    static delete(id) {
        return NoteModel.findByIdAndDelete(id);
    }

    static getCountOfAllNotes(userId, search_string) {
        const json_obj = {
            title: { '$regex': search_string, '$options': "i" } ,
            userId: userId,
            listId: null
        }
        return NoteModel.countDocuments(json_obj);
    }
}

module.exports = Note;