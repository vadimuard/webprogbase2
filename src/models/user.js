const mongoose = require("mongoose");

mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);

const UserSchema = new mongoose.Schema({
    email: { type: String },
    displayName: { type: String },
    role: { type: Number, default: 0 },
    registeredAt: { type: Date, default: Date.now() },
    avaUrl: { type: String, default: null },
    isDisabled: { type: Boolean, default: false },
    bio: { type: String }
});

const UserModel = mongoose.model('User', UserSchema);

class User {
    constructor(email, displayName, role, registeredAt, avaUrl, isDisabled, biography) {
        this.email = email;
        this.role = role;
        this.displayName = displayName;
        this.avaUrl = avaUrl;
        this.isDisabled = isDisabled;
        this.registeredAt = registeredAt;
        this.bio = biography;
    }

    static getById(id) {
        return UserModel.findById(id);
    }

    static getAllPaginated(page, search_string) {
        return UserModel.find({ "displayName": { "$regex": search_string, "$options": '$i' } }).skip(page * 8).limit(8);
    }

    static insert(user) {
        return new UserModel(user).save();
    }

    static update(user) {
        return UserModel.updateOne({ _id: user._id }, user);
    }

    static getByEmail(email) {
        return UserModel.findOne({ email });
    }

    static validateAuth(email) {
        return new Promise((resolve, reject) => {
            UserModel.findOne({ email: email })
                .then(user => {
                    if (!user) {
                        resolve(404);
                    }
                    else {
                        resolve(user);
                    }
                })
                .catch(err => {
                    reject(err);
                })
        });
    }

    static getCountOfAllUsers(search_string) {
        return UserModel.countDocuments({
            "displayName": { "$regex": search_string, "$options": '$i' }
        });
    }
}

module.exports = User;