const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  const username = (req.user) ? req.user.displayName : null;
  const role = (req.user) ? req.user.role : null;
  const user = (username) ? {username, role} : null;
  res.status(200).render("about", { title: "About Simply Notes", user });
});

module.exports = router;