const express = require('express');
const router = express.Router();
const busboyBodyParser = require('busboy-body-parser');

const cloudinary = require('cloudinary').v2;
const config = require('../config');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});
router.use(busboyBodyParser());

function checkLoggedUser(req, res, next) {
    if (!req.user) return res.redirect('/');
    next();
}

const list = require('../models/list');
const note = require('../models/note');

function saveFiles(images) {
    if (!images) {
        return new Promise((resolve, reject) => {
            resolve(null);
        });
    }
    let fileUrls = [];
    for (image of images) {
        if (!image) {
            fileUrls.push(null);
            continue;
        }
        const ext = image.substring(image.indexOf("/") + 1, image.indexOf(";base64"));
        const fileType = image.substring("data:".length, image.indexOf("/"));
        const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
        const rand = Math.ceil(Math.random() * 1000);
        const filename = `Photo_${Date.now()}_${rand}.${ext}`;
        const path = 'https://res.cloudinary.com/vadimuard/raw/upload/v1572734319/';
        fileUrls.push(path + filename);
        const conditions = {
            resource_type: 'raw',
            use_filename: true,
            unique_filename: false,
            public_id: filename
        }
        cloudinary.uploader.upload(image, conditions, (err, result) => {
            if (err) console.log(err);
            else console.log(result);
        });
    }
    return new Promise((resolve, reject) => {
        resolve(fileUrls);
    })
}

router.get('/update/:id', checkLoggedUser, (req, res) => {
    const id = req.params.id;
    let listId;
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    let json_obj = {
        notes: [],
        user
    };
    list.getById(id)
        .then(list => {
            listId = list._id;
            json_obj.id = id;
            json_obj.title = list.title;
            json_obj.text = list.text;
            json_obj.createdAt = list.createdAt;
            return note.getAllByList(listId)
        })
        .then(notes => {
            for (_note of notes) {
                json_child = {
                    title: _note.title,
                    id: _note._id
                }
                json_obj.notes.push(json_child);
            }
            res.render('lists/update', json_obj);
        })
        .catch(err => {
            console.log(err);
            res.status(404).send();
        })
})

router.post('/update/:id', checkLoggedUser, (req, res) => {
    const id = req.params.id;
    let updated_title = req.body.list_title;
    const updated_text = req.body.list_text;
    if (updated_title == "") updated_title = "untitled";
    list.getById(id)
        .then(_list => {
            _list.title = updated_title;
            _list.text = updated_text;
            return list.update(_list)
        })
        .then(updated_list => {
            res.redirect("/lists/" + id);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send();
        })
})

router.post('/delete/:id', checkLoggedUser, (req, res) => {
    const id = req.params.id.trim();
    list.delete(id)
        .then(list => {
            return note.deleteMany(id);
        })
        .then(notes => {
            res.redirect('/lists?page=1');
        })
        .catch(err => {
            console.log(err)
            res.status(404).send();
        });
});

router.post('/new', checkLoggedUser, (req, res) => {
    let new_list = new list();
    let users = req.body.users.split(',');
    if (!users) users = [];
    users = users.filter(item => item != req.user._id)
    for (let i = 0; i < users.length; i++) {
        users[i] = { _id: users[i] }
    }
    users.push({ _id: req.user._id });
    const title = req.body.title;
    if(title) new_list.title = title;
    new_list.text = req.body.description;
    new_list.usersGroup = users;
    list.insert(new_list)
        .then(result => {
            if (!result) {
                return res.sendStatus(500);
            }
            res.json(result._id)
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.get('/new', checkLoggedUser, (req, res) => {
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    res.render("lists/new", { title: "Create new list", user });
});

router.get('/:id', checkLoggedUser, (req, res) => {
    const id = req.params.id;
    let listId;
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    let json_obj = {
        notes: [], user
    };
    list.getById(id)
        .then(list => {
            listId = list._id;
            json_obj.id = id;
            json_obj.title = list.title;
            json_obj.text = list.text;
            json_obj.createdAt = list.createdAt;
            return note.getAllByList(listId)
        })
        .then(notes => {
            for (_note of notes) {
                json_child = {
                    title: _note.title,
                    id: _note._id
                }
                json_obj.notes.push(json_child);
            }
            res.render('list', json_obj);
        })
        .catch(err => {
            console.log(err);
            res.status(404).send();
        })
});

router.get('/', checkLoggedUser, (req, res) => {
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    res.render('lists', { title: "Lists of notes", user });
});

module.exports = router;