const express = require('express');
const busboyBodyParser = require('busboy-body-parser');
const bodyParser = require("body-parser");

const router = express.Router();
router.use(busboyBodyParser());

const cloudinary = require('cloudinary').v2;
const config = require('../config');

cloudinary.config({
    cloud_name: config.cloudinary.cloud_name,
    api_key: config.cloudinary.api_key,
    api_secret: config.cloudinary.api_secret
});

const note = require('../models/note');

function checkLoggedUser(req, res, next) {
    if (!req.user) return res.redirect('/');
    next();
}

router.post('/delete/:id', checkLoggedUser, (req, res) => {
    const id = req.params.id;
    note.delete(id)
        .then(note => {
            res.redirect('/notes?page=1');
        })
        .catch(err => {
            console.log(err)
            res.status(404).send();
        });
});

router.post('/new/:id', checkLoggedUser, (req, res) => {
    let title = req.body.note_title;
    let avaFile = req.file;
    let listId = req.params.id;
    if (listId === 'null') {
        listId = null;
    }
    if (!avaFile) avaFile = null;
    else avaFile = avaFile.filename;
    if (title.length == 0) {
        title = "untitled";
    }
    const text = req.body.note_text;
    let reminder;
    if (!req.body.reminder_date && !req.body.reminder_time) {
        reminder = null;
    } else if (!req.body.reminder_date || !req.body.reminder_time) {
        reminder = !req.body.reminder_date ? req.body.reminder_time : req.body.reminder_date;
    } else reminder = req.body.reminder_date + " " + req.body.reminder_time;
    const createdAt = new Date().toISOString();
    let new_note = new note();
    new_note.title = title;
    new_note.text = text;
    new_note.userId = req.user._id;
    new_note.listId = listId;
    new_note.reminder = reminder;
    new_note.createdAt = createdAt;
    new_note.authorId = Math.random().toFixed(0);
    let filePromised = new Promise((resolve, reject) => {
        if (!req.files.upload_file) {
            resolve(null);
        } else {
            cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, data) {
                if (error) { reject(error) } else { resolve(data.secure_url) }
            }).end(req.files.upload_file.data);
        }
    });
    filePromised.then(result => {
        new_note.fileUrl = result;
        return note.insert(new_note);
    })
        .then(inserted_note => {
            const id = inserted_note._id;
            res.redirect("/notes/" + id);
        })
        .catch(err => console.log(err));
});

router.get('/new/:id', checkLoggedUser, (req, res) => {
    const listId = req.params.id;
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    res.render('notes/new', { title: "Create new note", id: listId, user });
});

router.get('/:id', checkLoggedUser, (req, res) => {
    note.getById(req.params.id)
        .then(note => {
            if (!note) {
                res.status(404).send();
            } else {
                const username = (req.user) ? req.user.displayName : null;
                const role = (req.user) ? req.user.role : null;
                const user = (username) ? { username, role } : null;
                note.user = user;
                res.render("note", note);
            }
        })
        .catch(err => {
            console.log(err);
            res.status(404).send();
        });
});

router.get('/', checkLoggedUser, (req, res) => {
    const username = (req.user) ? req.user.displayName : null;
    const role = (req.user) ? req.user.role : null;
    const user = (username) ? { username, role } : null;
    res.render('notes', { title: "Your notes", user });
});

module.exports = router;