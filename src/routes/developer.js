const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('developer', {title: 'Developer info'});
});

module.exports = router;