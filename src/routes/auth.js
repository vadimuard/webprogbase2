const express = require('express');
const router = express.Router();
const passport = require('passport');
const url = require('url');
const util = require('util');
var querystring = require('querystring');

router.get('/login', passport.authenticate('auth0', { scope: 'openid email profile' }), function (req, res) {
    res.redirect('/');
});

router.get('/logout', (req, res) => {
    req.logout();
    let returnTo = req.protocol + '://' + req.hostname;
    const port = req.connection.localPort;
    if (port === 3000) {
        returnTo += ':' + port;
    }
    let logoutURL = new url.URL(
        util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
    );
    const searchString = querystring.stringify({
        client_id: process.env.AUTH0_CLIENT_ID,
        returnTo: returnTo
    });
    logoutURL.search = searchString;
    res.redirect(logoutURL);
});

module.exports = router;