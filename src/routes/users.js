const express = require('express');
const User = require('../models/user');
const router = express.Router();

function checkAdmin(req, res, next) {
  if (!req.user) return res.redirect('/');
  if (req.user.role !== 1) return res.sendStatus(403);
  next();
}

router.get('/:id', checkAdmin, (req, res) => {
  User.getById(req.params.id)
    .then(_user => {
      const username = (req.user) ? req.user.displayName : null;
      const role = (req.user) ? req.user.role : null;
      const admin = (username) ? { username, role } : null;
      _user.user = admin;
      _user.title = username;
      res.render("user", _user);
    })
    .catch(err => {
      console.log(err);
      res.status(404).send();
    })
});

router.post('/:id', (req, res) => {
  if (!req.user) return res.redirect('/');
  const id = req.params.id;
  if (req.user._id == id) {
    const bio = req.body.bio;
    const username = req.body.username;
    const user = {
      bio, displayName: username, _id: id
    };
    User.update(user)
      .then(result => {
        if (result.n === 0) {
          res.sendStatus(500);
        } else {
          req.user.displayName = username;
          req.user.bio = bio;
          res.redirect('/profile');
        }
      })
  } else {
    res.redirect('/');
  }
})

router.get('/update/:id', (req, res) => {
  if (!req.user) return res.redirect('/');
  const id = req.params.id;
  if (req.user._id == id) {
    res.render('users/update', { title: req.user.displayName, user: { _id: req.user._id, username: req.user.displayName, bio: req.user.bio } });
  } else {
    res.redirect('/');
  }
})

router.get('/', checkAdmin, (req, res) => {
  const username = (req.user) ? req.user.displayName : null;
  const role = (req.user) ? req.user.role : null;
  const user = (username) ? { username, role } : null;
  res.render("users", { title: "Users", user });
});

module.exports = router;
