const express = require('express');
const router = express.Router();
const user = require('../models/user');
const note = require('../models/note');
const list = require('../models/list');
const passport = require('passport');
const cryptoJS = require('crypto-js');

const path = require('path');

const page_size = 8;

function checkAdmin(req, res, next) {
    if (!req.user) {
        res.redirect('/');
    } else {
        if (req.user.role == 0) res.sendStatus(403);
        else next();
    }
}

function checkUser(req, res, next) {
    if (!req.user)
        res.redirect('/');
    else
        next();
}

router.post("/lists", checkUser, (req, res) => {
    let data = req.query;
    list.insert(data)
        .then(result => {
            if (!result) {
                res.sendStatus(500);
            } else {
                res.status(201).json(result);
            }
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.delete("/lists/:id", checkUser, (req, res) => {
    const id = req.params.id;
    list.delete(id)
        .then(result => {
            if (!result) {
                res.sendStatus(404);
                return;
            }
            res.json(result);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.put("/lists/:id", checkUser, (req, res) => {
    let data = req.query;
    data._id = req.params.id;
    list.update(data)
        .then(result => {
            if (result.n === 0) {
                res.sendStatus(404);
                return false;
            }
            else {
                return list.getById(req.params.id)
            }
        })
        .then(result => {
            if (result !== false) {
                res.json(result);
            }
        })
        .catch(err => {
            res.sendStatus(500);
            return false;
        })
})

router.post("/notes", checkUser, (req, res) => {
    note.insert(data)
        .then(result => {
            if (!result) {
                res.sendStatus(500);
            } else {
                res.status(201).json(result);
            }
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.delete("/notes/:id", checkUser, (req, res) => {
    const id = req.params.id;
    note.delete(id)
        .then(result => {
            if (!result) {
                res.sendStatus(404);
                return;
            }
            res.json(result);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.put("/notes/:id", checkUser, (req, res) => {
    let data = req.query;
    data._id = req.params.id;
    note.update(data, req.params.id)
        .then(result => {
            if (result.n === 0) {
                res.sendStatus(404);
                return false;
            }
            else {
                return note.getById(req.params.id)
            }
        })
        .then(result => {
            if (result !== false) {
                res.json(result);
            }
        })
        .catch(err => {
            res.sendStatus(500);
            return false;
        })
})

router.post("/users", checkUser, (req, res) => {
    let data = req.query;
    if (data.login.length < 4) {
        res.status(400).send('username is invalid');
        return;
    }
    if (data.password.length < 8 || data.password.length > 40) {
        res.status(400).send('password is invalid');
        return;
    }
    if (data.fullname.length < 4 || data.password.length > 40) {
        res.status(400).send('fullname is invalid');
        return;
    }
    if (data.dupl_pass !== data.password) {
        res.status(400).send('passwords do not coinside');
        return;
    }
    delete data.dupl_pass;
    user.isUsernameUnique(data.login)
        .then(result => {
            if (!result) {
                res.status(400).send('username is invalid or already in use');
                return false;
            }
            data.passwordHash = cryptoJS.MD5(data.password);
            return user.insert(data);
        })
        .then(result => {
            if (!result) {
                res.sendStatus(500);
            } else {
                res.status(201).json(result);
            }
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.get('/user/:email', checkUser, (req, res) => {
    let email = req.params.email;
    if (!email) return res.sendStatus(404);
    user.getByEmail(email)
        .then(result => {
            if (!result) return res.sendStatus(404);
            res.json({
                user: result.displayName,
                id: result._id
            });
        })
})

router.delete("/users/:id", checkUser, (req, res) => {
    const id = req.params.id;
    user.delete(id)
        .then(result => {
            if (!result) {
                res.sendStatus(404);
                return;
            }
            res.json(result);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.put("/users/:id", checkUser, (req, res) => {
    let data = req.body;
    data._id = req.params.id;
    if (req.user._id != data._id && !(Object.keys(data).length == 2 && data.role)) {
        res.sendStatus(403);
        return;
    }

    user.update(data)
        .then(result => {
            if (result === false) return false;
            if (result.n === 0) {
                res.sendStatus(404);
                return false;
            }
            return user.getById(req.params.id)
        })
        .then(result => {
            if (result !== false) {
                res.json(result);
            }
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.get("/lists", checkUser, (req, res) => {
    let search = req.query.search;
    const page = parseInt(req.query.page) - 1;
    if (!search) search = "";
    if ((!page && page !== 0) || page < 0) {
        res.redirect('lists?page=1&search=' + search);
        return;
    }
    list.getAllPaginated(req.user._id, page, search)
        .then(lists => {
            res.json(lists);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.get("/lists/count", checkUser, (req, res) => {
    let search = req.query.search;
    if (!search) search = "";
    list.getCountOfAllLists(req.user._id, search)
        .then(count => {
            let pages_count;
            if (count % page_size == 0 && count !== 0) {
                pages_count = count / page_size;
                pages_count = pages_count.toFixed(0);
            } else {
                pages_count = count / page_size + 1;
                pages_count = Math.floor(pages_count);
            }
            let pages = [];
            for (let i = 0; i < pages_count; i++) {
                pages[i] = { val: i + 1, isActive: false };
            }
            res.json(pages);
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500);
        })
})

router.get("/lists/:id", checkUser, (req, res) => {
    const id = req.params.id;
    list.getById(id)
        .then(_list => {
            res.send(_list);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.get('/users', checkAdmin, (req, res) => {
    const page = parseInt(req.query.page) - 1;
    let search = req.query.search;
    if (!search) search = "";
    if ((!page && page !== 0) || page < 0) {
        res.redirect('users?page=1&search=' + search);
        return;
    }
    user.getAllPaginated(page, search)
        .then(users => {
            res.send(users);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.get('/users/count', checkAdmin, (req, res) => {
    let search = req.query.search;
    if (!search) search = "";
    user.getCountOfAllUsers(search)
        .then(count => {
            let pages_count;
            if (count % page_size == 0 && count !== 0) {
                pages_count = count / page_size;
                pages_count = pages_count.toFixed(0);
            } else {
                pages_count = count / page_size + 1;
                pages_count = Math.floor(pages_count);
            }
            let pages = [];
            for (let i = 0; i < pages_count; i++) {
                pages[i] = { val: i + 1, isActive: false };
            }
            res.json(pages);
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500);
        })
})

router.get('/users/:id', checkAdmin, (req, res) => {
    const id = req.params.id;
    user.getById(id)
        .then(_user => {
            res.json(_user);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.get('/notes', checkUser, (req, res) => {
    const page = parseInt(req.query.page) - 1;
    let search = req.query.search;
    if (!search) search = "";
    if ((!page && page !== 0) || page < 0) {
        res.redirect('notes?page=1&search=' + search);
        return;
    }
    note.getAllPaginated(req.user._id, page, search)
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
});

router.get('/notes/count', checkUser, (req, res) => {
    let search = req.query.search;
    if (!search) search = "";
    note.getCountOfAllNotes(req.user._id, search)
        .then(count => {
            let pages_count;
            if (count % page_size == 0 && count !== 0) {
                pages_count = count / page_size;
                pages_count = pages_count.toFixed(0);
            } else {
                pages_count = count / page_size + 1;
                pages_count = Math.floor(pages_count);
            }
            let pages = [];
            for (let i = 0; i < pages_count; i++) {
                pages[i] = { val: i + 1, isActive: false };
            }
            res.json(pages);
        })
        .catch(err => {
            console.log(err)
            res.sendStatus(500);
        })
});


router.get("/notes/:id", checkUser, (req, res) => {
    const id = req.params.id;
    note.getById(id)
        .then(_note => {
            if (!_note) {
                res.sendStatus(404);
            }
            res.json(_note);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

router.get('/', (req, res) => {
    res.json({});
});

router.get('/me', checkUser, (req, res) => {
    res.json(req.user);
})

router.get('/auth_config', (req, res) => {
    res.sendFile(path.join(__dirname, '../auth_config.json'));
});

module.exports = router;