const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/', (req, res) => {
  const username = (req.user) ? req.user.displayName : null;
  const role = (req.user) ? req.user.role : null;
  const user = (username) ? {username, role} : null;
  res.status(200).render("index", { title: "Simply Notes",  user: user });
});

module.exports = router;