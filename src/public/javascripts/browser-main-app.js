const logOut = document.getElementById('logout-form');
const logIn = document.getElementById('login-form');

let links = document.getElementsByTagName('a');

window.addEventListener('load', () => {
    for (let i = 0; i < links.length; i++) {
        links[i].addEventListener('click', ev => {
            const href = links[i].href;
            if (!href || href !== "#") {
                const jwt = localStorage.getItem('jwt');
                let reqOptions = {
                    headers: {
                        Authorization: `Bearer ${jwt}`
                    }
                };
                if (!jwt) {
                    reqOptions = {};
                }
                fetch(href, reqOptions);
            }
        })
    }
})

// if (logIn !== null)
//     logIn.addEventListener('submit', ev => {
//         const formData = new FormData(ev.target);
//         const bodyData = new URLSearchParams(formData);
//         ev.preventDefault();
//         fetch("/auth/login", { method: "POST", body: bodyData })
//             .then(x => x.json())
//             .then(authResult => {
//                 const jwt = authResult.token;
//                 localStorage.setItem("jwt", jwt);
//                 window.location.href = "/";
//             })
//             .catch(console.error)
    // })
// if (logOut !== null)
//     logOut.addEventListener('submit', e => {
//         // e.preventDefault();
//         localStorage.removeItem('jwt');
//         // window.location.href = "/";
//     })