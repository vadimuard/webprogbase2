const users_list = document.getElementById('users-list');
const search = document.getElementById('search-field');
const search_form = document.getElementById('search_bar');
const search_info = document.getElementById('search-info');
const page_items = document.getElementsByClassName('page-item');
const pagination = document.getElementById('pagination');

let cur_page = 1;

search.addEventListener('input', ev => {
    if(search.value.length > 30){
        search.value = search.value.slice(0, 31);
    }
    addPagination(search.value);
    search_info.innerHTML = "Our Users";
});

window.addEventListener('load', async ev => {
    addPagination('');
})

search_form.addEventListener('submit', ev => {
    if(search.value.length > 30){
        search.value = search.value.slice(0, 31);
    }
    ev.preventDefault();
    const new_search_info = `Users found after searching for '${search.value}'`;
    search_info.innerHTML = new_search_info;
    addPagination(search.value);
})

async function getNotes(page, search) {
    Promise.all([
        fetch("/api/v1/users?page=" + page + "&search=" + search).then(x => x = x.json()),
        fetch("../templates/users.mst").then(x => x = x.text())
    ])
        .then(([data, template]) => {
            let renderedHtmlText;
            if (!data || data.length === 0) {
                if(search)
                    renderedHtmlText = `Your search '${search}' did not match any usernames`;
                else 
                    renderedHtmlText = 'List of users is empty. Maybe smth is wrong?';
            } else {
                renderedHtmlText = Mustache.render(template, { users: data });
            }
            users_list.innerHTML = renderedHtmlText;
        })
        .catch(err => console.error(err));
}

function addPageBtnListeners() {
    for (let i = 0; i < page_items.length; i++) {
        page_items[i].addEventListener("click", ev => {
            if (i !== 0 && i !== page_items.length - 1) {
                cur_page = page_items[i].value;
            } else if (i === 0) cur_page -= 1;
            else if (i === page_items.length - 1) cur_page += 1;
            addPagination(search.value);
        });
    }
}

function addPagination(search) {
    Promise.all(([
        fetch("/api/v1/users/count?search=" + search).then(x => x = x.json()),
        fetch("/templates/pagination.mst").then(x => x = x.text())
    ]))
        .then(([pages, template]) => {
            if (cur_page < 1 || cur_page > pages.length) cur_page = (cur_page < 1) ? 1 : pages.length;
            const isPrevNull = (cur_page === 1) ? true : false;
            const isNextNull = (cur_page === pages.length) ? true : false;
            pages[cur_page - 1].isActive = true;
            let renderedHtmlText = "";
            if(pages.length > 1)
                renderedHtmlText = Mustache.render(template, { pages, isPrevNull, isNextNull });
            pagination.innerHTML = renderedHtmlText;
            addPageBtnListeners();
            getNotes(cur_page, search);
        })
}