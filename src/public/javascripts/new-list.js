const searchUser = document.getElementById('searchUser');
const userEmail = document.getElementById('userEmail');
const userInfo = document.getElementById('userInfo');
const clearBtn = document.getElementById('clearBtn');
const submitUser = document.getElementById('addUser');
const submitForm = document.getElementById('listCreated');
const usersList = document.getElementById('usersList');

let usersGroup = [];
let usernames = [];
let tmpUserId;
let tmpUsername;

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: usernames
        }
    }
    render() {
        return (
            <ul className="list-group list-group-flush mt-3">
                {
                    this.state.items.map((item, i) =>
                        <li className="list-group list-group-flush-item p-2" key={i}>{item}</li>
                    )
                }
            </ul>
        )
    }
}

submitForm.addEventListener('submit', (ev) => {
    ev.preventDefault();
    const formData = new FormData(ev.target);
    formData.append('users', usersGroup.join());
    const bodyData = new URLSearchParams(formData);
    fetch('/lists/new', {
        method: 'POST',
        body: bodyData
    })
        .then(x => x = x.json())
        .then(x => {
            if (x)
                window.location.href = "/lists/" + x;
            else
                window.location.href = "/lists"
        .catch(err => console.log(err))
    })
})

searchUser.addEventListener('click', ev => {
    let email = userEmail.value;
    userInfo.hidden = true;
    userInfo.innerHTML = '';
    if (!email) {
        clear();
        return;
    }
    const request = '/api/v1/user/' + email;
    fetch(request)
        .then(x => { if (x.status === 200) return x.json(); else clear() })
        .then(res => {
            if (res) {
                tmpUserId = res.id;
                tmpUsername = res.user;
                userInfo.hidden = false;
                userInfo.innerHTML = tmpUsername;
            }
        })
})


submitUser.addEventListener('click', ev => {
    if (tmpUserId && !usersGroup.find(item => item === tmpUserId)) {
        usersGroup.push(tmpUserId);
        usernames.push(tmpUsername);
        addUser();
    }
    $('#userGroup').modal('hide');
    clear();
})

clearBtn.addEventListener('click', clear);

function clear() {
    tmpUserId = '';
    tmpUsername = '';
    userInfo.hidden = true;
    userInfo.innerHTML = '';
    userEmail.value = '';
}

function addUser() {
    ReactDOM.render(<List />, usersList);
}