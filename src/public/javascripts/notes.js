const notes_list_1 = document.getElementById('notes-list-1');
const notes_list_2 = document.getElementById('notes-list-2');
const search = document.getElementById('search-field');
const search_form = document.getElementById('search_bar');
const search_info = document.getElementById('search-info');
const page_items = document.getElementsByClassName('page-item');
const pagination = document.getElementById('pagination');

let cur_page = 1;

search.addEventListener('input', ev => {
    if (search.value.length > 30) {
        search.value = search.value.slice(0, 31);
    }
    addPagination(search.value);
    search_info.innerHTML = "Your notes";
});

window.addEventListener('load', async ev => {
    addPagination('');
})

search_form.addEventListener('submit', ev => {
    ev.preventDefault();
    if (search.value.length > 30) {
        search.value = search.value.slice(0, 31);
    }
    const new_search_info = `Notes found after searching for '${search.value}'`;
    search_info.innerHTML = new_search_info;
    addPagination(search.value);
})

function getNotes(page, search) {
    return Promise.all([
        fetch("/api/v1/notes?page=" + page + "&search=" + search).then(x => x = x.json()),
        fetch("../templates/notes.mst").then(x => x = x.text())
    ])
        .then(([data, template]) => {
            let renderedHtmlText = [];
            let state = false;
            if (!data || data.length === 0) {
                if(search) renderedHtmlText[0] = `Your search '${search}' did not match any notes`;
                else renderedHtmlText[0] = "Your list of notes is empty. Create new one by clicking button";
            } else {
                const one_day = 1000 * 3600 * 24;
                data.forEach(item => {
                    const present_day = new Date();
                    const createdAt = new Date(item.createdAt);
                    item.createdAt = Math.round((present_day.getTime() - createdAt.getTime())/one_day);                    
                });
                if(data.length > 4){
                    renderedHtmlText[0] = Mustache.render(template, { notes: data.slice(0, 4) })
                    renderedHtmlText[1] = Mustache.render(template, {notes: data.slice(4)});
                }else renderedHtmlText[0] = Mustache.render(template, {notes: data});
                state = true;
            }
            notes_list_1.innerHTML = renderedHtmlText[0];
            notes_list_2.innerHTML = renderedHtmlText[1] ? renderedHtmlText[1] : '';
            return state;
        })
        .catch(err => { console.error(err); return false; });
}

function addPageBtnListeners() {
    for (let i = 0; i < page_items.length; i++) {
        page_items[i].addEventListener("click", ev => {
            if (i !== 0 && i !== page_items.length - 1) {
                cur_page = page_items[i].value;
            } else if (i === 0) cur_page -= 1;
            else if (i === page_items.length - 1) cur_page += 1;
            addPagination(search.value);
        });
    }
}

function addPagination(search) {
    Promise.all(([
        fetch("/api/v1/notes/count?search=" + search).then(x => x = x.json()),
        fetch("/templates/pagination.mst").then(x => x = x.text())
    ]))
        .then(([pages, template]) => {
            if (cur_page < 1 || cur_page > pages.length) cur_page = (cur_page < 1) ? 1 : pages.length;
            const isPrevNull = (cur_page === 1) ? true : false;
            const isNextNull = (cur_page === pages.length) ? true : false;
            pages[cur_page - 1].isActive = true;
            let renderedHtmlText = "";
            if(pages.length > 1) 
                renderedHtmlText = Mustache.render(template, { pages, isPrevNull, isNextNull });
            const res = getNotes(cur_page, search);
            if (res) {
                pagination.innerHTML = renderedHtmlText;
                addPageBtnListeners();
            }
        })
}