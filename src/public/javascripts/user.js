const userRole = document.getElementById('changeUserRole');

userRole.addEventListener('change', ev=>{
    const formData = new FormData();
    formData.append('role', ev.target.value);
    const bodyData = new URLSearchParams(formData);
    fetch('/api/v1/users/' + window.location.href.split('/users/')[1], {
        method: 'PUT',
        body: bodyData
    })
    .then(x => x = x.json())
    .then(x => console.log(x))
    .catch(console.error);
})