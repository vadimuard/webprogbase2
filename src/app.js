const express = require('express');
const app = express();
const path = require('path');
const mustache = require('mustache-express');
const dotenv = require('dotenv');
const config = require('./config');
const session = require('express-session');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const User = require('./models/user');
const Note = require('./models/note');
const index_route = require('./routes/index');
const notes_route = require('./routes/notes');
const lists_route = require('./routes/lists');
const about_route = require('./routes/about');
const users_route = require('./routes/users');
const auth_route = require('./routes/auth');
const dev_route = require('./routes/developer');
const api_route = require('./routes/api');
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const cookieParser = require('cookie-parser');

dotenv.config();
const dbUrl = config.dbUrl;
const connectOptions = { useNewUrlParser: true };
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', true);
app.use(methodOverride());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
mongoose.connect(dbUrl, connectOptions)
  .then(() => console.log('DB connected: ' + dbUrl))
  .catch(err => console.log("DB was not connected\n" + err));
const VIEWS_PATH = path.join(__dirname, "/views");
app.engine('mst', mustache(path.join(VIEWS_PATH, '/partials')));

passport.use(new Auth0Strategy(
  {
    domain: process.env.AUTH0_DOMAIN,
    clientID: process.env.AUTH0_CLIENT_ID,
    clientSecret: process.env.AUTH0_CLIENT_SECRET,
    callbackURL: process.env.AUTH0_CALLBACK_URL || 'http://localhost:3000/callback'
  },
  function (accessToken, refreshToken, extraParams, profile, done) {
    return done(null, profile);
  }
));

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

app.use(cookieParser());

var sess = {
  secret: 'ole_nakickasfn_mdlas_fdnndkm',
  cookie: {},
  resave: false,
  saveUninitialized: true
};

if (app.get('env') === 'production') {
  sess.cookie.secure = true;
  app.set('trust proxy', 1);
}
app.use(session(sess));

app.use(function (req, res, next) {
  res.locals.user = req.user;
  next();
})

app.use(passport.initialize());
app.use(passport.session());

app.get('/callback', function (req, res, next) {
  passport.authenticate('auth0', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login'); }
    const data = user;
    const email = data.emails[0].value;
    User.validateAuth(email)
      .then(result => {
        if (result === 404) {
          let newUser = new User();
          if (data.provider === "auth0") {
            newUser.displayName = data.nickname;
          } else if (data.provider === "google-oauth2") {
            newUser.avaUrl = data.picture;
            newUser.displayName = data.displayName;
          } else {
            res.sendStatus(500);
          }
          newUser.email = email;
          return User.insert(newUser);
        } else {
          return result;
        }
      })
      .then(user => {
        req.logIn(user, (err) => {
          if (err) {
            res.sendStatus(500);
          } else {
            res.redirect('/');
          }
        })
      })
      .catch(err => {
        console.log(err);
        res.sendStatus(500);
      })
  })(req, res, next);
});

app.use('/auth', auth_route);
app.use('/notes', notes_route);
app.use('/users', users_route);
app.use('/lists', lists_route);
app.use('/about', about_route);
app.use('/index', index_route);
app.use('/api/v1', api_route);
app.use('/developer/v1', dev_route);
app.use('/', index_route);

app.set('view engine', 'mst');
app.set('views', path.join(__dirname, 'views'));
app.use(express.static('public'));
app.use(express.static("data"));

app.get('/profile', (req, res) => {
  if (!req.user) return res.redirect('/');
  const username = req.user.displayName;
  const bio = req.user.bio;
  const email = req.user.email;
  const registeredAt = new Date(req.user.registeredAt).toDateString();
  const avaUrl = req.user.avaUrl;
  const user = { username, bio, email, registeredAt, _id: req.user._id, avaUrl };
  res.render('profile', { title: req.user.displayName, user });
})

app.get('*', function (req, res) {
  res.render('not-found', { title: 'Not found' });
});

module.exports = app;